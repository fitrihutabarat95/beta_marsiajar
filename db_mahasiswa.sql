/*
SQLyog Ultimate v8.55 
MySQL - 5.5.5-10.1.31-MariaDB : Database - db_mahasiswa
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_mahasiswa` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_mahasiswa`;

/*Table structure for table `tbl_dosen` */

DROP TABLE IF EXISTS `tbl_dosen`;

CREATE TABLE `tbl_dosen` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `matkul` varchar(100) NOT NULL,
  `no_hp` varchar(12) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_dosen` */

insert  into `tbl_dosen`(`id`,`nama`,`matkul`,`no_hp`) values (1,'Dua Mata Saya','karya pompi s','085811223341'),(2,'Dua Mata Saya','karya pompi s','085811223342'),(3,'NAIK DELMAN','karya  Pak Kasur','085811223343'),(4,'Aku anak indonesia','karya A.T Mahmud','085811223344'),(5,'Ambilkan Bulan Bu','karya A.T Mahmud','085811223345'),(6,'Burung Kutilang','karya Ibu Sud','085811223346'),(7,'Tik tik tik bunyi hujan','karya ibu sud','085811223347'),(8,'Kasih ibu','karya SM.Muchtar','085811223348'),(9,'Kupu kupu ','karya ibu sud','085811223349'),(10,'Halo-Halo Bandung','karya Ismail Marzuki','085811223350');

/*Table structure for table `tbl_matkul` */

DROP TABLE IF EXISTS `tbl_matkul`;

CREATE TABLE `tbl_matkul` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nama_dosen` varchar(100) NOT NULL,
  `matkul` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_matkul` */

insert  into `tbl_matkul`(`id`,`nama_dosen`,`matkul`) values (15,'sakit','sick'),(17,'makan','eat'),(18,'jalan','walk'),(20,'jalan','walk'),(21,'tas','bag'),(22,'kabel','cable'),(23,'kabel','cable');

/*Table structure for table `tbl_user` */

DROP TABLE IF EXISTS `tbl_user`;

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(23) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `encrypted_password` varchar(80) NOT NULL,
  `salt` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_user` */

insert  into `tbl_user`(`id`,`unique_id`,`nama`,`email`,`encrypted_password`,`salt`) values (0,'5e06cd7ddb6102.13283545','fitri','fir@gmail.com','N/s0cqy6ExII9b5+tVp4btXJmSw2MjMxNDkzNjcw','6231493670'),(0,'5e06dbb680b643.82738820','fit','q','ki5jyV5vtRaglkjK1zCS3MY/4nU3YTk1Y2U4NTIy','7a95ce8522'),(0,'5e0731859e4a84.06323303','fitri hutabarat','fitri@gmail.com','pNhNhexCaMoq9z7xEJ5SWCz9TYE1NTkyZGIzMGY3','5592db30f7'),(0,'5e079bfd77ff55.10878878','fitri h','f@g','fzYB375oyJT6owm+GLAMzRlSXkRlZjIzNWE1MTFj','ef235a511c'),(0,'5e08bd0b7fb132.37247720','fitri hutabarat','a','adtleRHwAXJ9gEoBARGwfEv1QsRmYjg0MmM2YjQ2','fb842c6b46'),(0,'5e09f5f8a69ef8.21449441','bintang hutabarat','a1','yO1sd3tTVr/668bj+kW3WARNjPwyNGM4OGI4MjUx','24c88b8251'),(0,'5e11a9ac797d69.45162046','fitri hutbarat','s','nnRFGaZrb2jcaRtsU6EW2el21LYwMWM5MGI1MGVm','01c90b50ef'),(0,'5e11e34d25e8f2.73069483','hernae sihombing','sis','UfTJ6mPoHcBbDWEZ/EKYhsZUyTs4MGVhODhmZDRh','80ea88fd4a'),(0,'5e1ef44c7c0d74.44918535','h','h','Hvizyk6TfaJfE5LHFEDFYfgsuesxZDVmNDc4OTJi','1d5f47892b'),(0,'5e211e6c268210.50931598','rafita','raf','1TA+tOGAZcHMSie+vdTJ1WZu5m45ZjI5ZDdhMzcw','9f29d7a370'),(0,'5e21331d607bd4.20985823','fitrihutabarat2','ab','KdP4s3+H+mtIaRoVcxTY7SCv3HQxMmFlNjExMjVj','12ae61125c');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
